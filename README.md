Rabbitmq_cluster role
=========

Installs rabbitmq on several nodes and form cluster
It can:
  - replace default guest administrator user
  - enable/disalbe rabbitmq_management plugin
It can NOT:
  - add/remove new user accounts
  - enable rabbitmq plugins(except for rabbitmq_management)

Requirements
------------

—

Role Variables
--------------

See defaults/main.yml

Dependencies
------------

—

Example Playbook
----------------

```
---
- name: Install rabbitmq
  hosts: rabbitmq_vms
  become: true
  gather_facts: true
  roles:
    - rabbitmq_cluster
      tags:
        - role_rabbitmq_cluster
```

License
-------

MIT

Author Information
------------------

n98gt56ti@gmail.com
